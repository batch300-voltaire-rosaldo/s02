<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02 Activity Selection Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Array Manipulation</h1>

    <?php array_push($students, 'John Smith'); ?>
    <p><?php print_r($students); ?></p>
    <p><?php echo count($students); ?></p>
    
    <?php array_push($students, 'Jane Smith'); ?>
    <p><?php print_r($students); ?></p>
    <p><?php echo count($students); ?></p>

    <?php array_shift($students); ?>
    <p><?php print_r($students); ?></p>
    <p><?php echo count($students); ?></p>

    <h1>Divisibles by Five</h1>

    <?php modifiedForLoop(); ?>

</body>
</html>