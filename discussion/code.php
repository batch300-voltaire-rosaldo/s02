<?php

//[1] Repetition Control Structures
//Repetition control structures are used to execute code multiple times

//While Loop
//while loop - takes a single condition
//if the condition evaluates to true, the code inside the block will run

function whileLoop(){

	$count = 5;

	while($count !==0){

		echo $count . '<br/>';
		$count--;
	}
}

//Do-While Loop
//do-while loop - it works a lot like the while loop, but unlike while loops, do while loop guarantee that the code will be executed at least once

function doWhileLoop(){

	$count = 20;

	do {
		echo $count.'<br/>';
		$count--;
	}while($count > 0);

}

//For Loop
/*
	for loop is more flexible than while and do-while loops
	consists of 3 parts:
	1. initial value
	2. condition
	3. iteration

*/
function forLoop() {

	for($count = 0; $count <=20; $count++){
		echo $count.'<br/>';
	}

}

//Preferred usage of While vs For Loops
//For loops are best used when the number of iteration is known beforehand (iterating over arrays)
//While Loops - are better when number of iterations is unknown at the time the loop is defined (depends on user input)


//Continue and Break statements
/*
	Continue is a keyword that allows the code to go to the next loop without finishing the current code block
	Break is a keyword taht ends the execution of the current loop
*/

function modifiedForLoop() {

	for ($count = 0; $count <=20; $count++){
		if($count % 2 === 0){
			continue;
		}
		echo $count.'<br/>';
		if($count>10){
			break;
		}
	}
}

//[2] Array Manipulation
//An array is a kind of variable that can hold more than one value
//Arrays are declared using the square brackets '[]' or array() function

$studentNumbers = array('2020-1923', '2020-1924','2020-1925','2020-1926','2020-1927'); //before PHP 5.4
$studentNumbers = ['2020-1923', '2020-1924','2020-1925','2020-1926','2020-1927']; //introduced on PHP 5.4

//Simple Arrays
$grades = [98.5,94.3,89.2,90.1];
$computerBrands = ['Acer','Asus','Lenovo','Neo','Redfox','Gateway','Toshiba','Fujitsu','Dell','HP'];
$task = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake react'
];

//Associative Array
$gradePeriods = ['firstGrading' => 98.5,'secondGrading' => 94.3, 'thirdGrading' =>89.2, 'fourthGrading' =>90.1];

//Two-Dimensional Array

$heroes = [
	['ironman','thor','hulk'],
	['wolverine','cyclops','jean grey'],
	['batman','superman','wonderwoman']
];

//2-dimensional associative array
$ironManPowers = [
	'regular' => ['repulsor blast','rocket punch'],
	'signature' => ['unibeam']
];

//Array Mutations - seek to modify the contents of an array while array iterations aim to evaluate each element in an array

//Array Sorting

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);

function searchBrand($brands, $brand){
	return (in_array($brand, $brands)) ? "$brand is in the array." : "$brand is not in the array.";
}

$reversedGradePeriods = array_reverse($gradePeriods);